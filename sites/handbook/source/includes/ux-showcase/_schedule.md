[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date ('22) | Host          | Presenter 1     | Presenter 2     | Presenter 3      | 
| ---------- | ------------- | --------------- | --------------- | ---------------- | 
| 8/3        | Justin        | Matt Nearents   | Amelia Bauerly  | Daniel Mora      | 
| 8/17       | Taurie        | Alexis Ginsberg | Emily Sybrant   | Matej Latin      | 
| 8/31       | TBD (APAC)    | Katie Macoy     | Michael Le      |                  | 
| 9/14       | Marcel        | Nick Brandt     | Emily Bauman    | Nick Leonard     | 
| 9/28       | Rayana        | Sunjung Park    | Michael Fangman | Camellia X Yang  | 
| 10/12      | Blair         | Matt Nearents   | Becka Lippert   | Philip Joyce     | 
| 10/26      | Jacki         | Alexis Ginsberg | Libor Vanc      | Andy Volpe       | 
| 11/9       | Justin        | Matej Latin     | Kevin Comoli    | Ali Ndlovu       | 
| 11/23/2022 | Taurie        |                 |                 |                  | 
| 12/7       | TBD (APAC)    |                 |                 |                  | 
| 12/21      | Marcel        |                 |                 |                  | 
